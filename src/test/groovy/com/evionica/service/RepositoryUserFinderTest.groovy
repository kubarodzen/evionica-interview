package com.evionica.service

import com.evionica.dto.User
import com.evionica.repository.UserRepository
import spock.lang.Shared
import spock.lang.Specification

class RepositoryUserFinderTest extends Specification {

    def userRepository = Mock(UserRepository)
    def underTest = new RepositoryUserFinder(userRepository)

    @Shared
    def johnBrownOne = createUser('John', 'Brown', 'john@brown.test')

    @Shared
    def userOther = createUser('John', 'Other', 'john@other.test')

    @Shared
    def johnBrownTwo = createUser('John', 'Brown Two', 'john@brown.test')

    @Shared
    def userNoEmail = createUser('John', 'Brown Two', null)

    def "should find user by email"() {
        when:
        def result = underTest.findUserByEmail(email)

        then:
        1 * userRepository.findAll() >> [johnBrownOne, userOther, johnBrownTwo, userNoEmail]

        result == expectedResult

        where:
        email              | expectedResult
        'john@brown.test'  | Optional.of(johnBrownTwo)
        'john@other.test'  | Optional.of(userOther)
        'some'             | Optional.empty()
        ''                 | Optional.empty()
    }

    def createUser(firstName, lastName, email) {
        return new User(firstName, lastName, email, '', '', '', '', '', '', '')
    }
}
