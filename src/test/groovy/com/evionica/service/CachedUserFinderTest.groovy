package com.evionica.service

import com.evionica.dto.User
import spock.lang.Specification

class CachedUserFinderTest extends Specification {

    def repositoryUserFinder = Mock(RepositoryUserFinder)

    def underTest = new CachedUserFinder(repositoryUserFinder)

    def "should find user by email from repository"() {
        given:
        def user = createUser('John', 'Brown', 'john@brown.test')

        when:
        def result = underTest.findUserByEmail('john@brown.test')

        then:
        1 * repositoryUserFinder.findUserByEmail('john@brown.test') >> Optional.of(user)

        expect:
        result.get() == user
    }

    def createUser(firstName, lastName, email) {
        return new User(firstName, lastName, email, '', '', '', '', '', '', '')
    }

}
