package com.evionica.dto;

public class User {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String phone;
    private final String addrStreet;
    private final String addrCity;
    private final String addrPostCode;
    private final String deliveryStreet;
    private final String deliveryCity;
    private final String deliveryPostCode;

    public User(String firstName, String lastName, String email, String phone,
                String addrStreet, String addrCity, String addrPostCode,
                String deliveryStreet, String deliveryCity, String deliveryPostCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.addrStreet = addrStreet;
        this.addrCity = addrCity;
        this.addrPostCode = addrPostCode;
        this.deliveryStreet = deliveryStreet;
        this.deliveryCity = deliveryCity;
        this.deliveryPostCode = deliveryPostCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddrStreet() {
        return addrStreet;
    }

    public String getAddrCity() {
        return addrCity;
    }

    public String getAddrPostCode() {
        return addrPostCode;
    }

    public String getDeliveryStreet() {
        return deliveryStreet;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public String getDeliveryPostCode() {
        return deliveryPostCode;
    }
}
